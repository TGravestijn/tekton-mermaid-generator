# Python Tekton pipeline Mermaid generator
import os
import sys
import yaml
from yaml.loader import SafeLoader

pipelineFileName = str(sys.argv[1])

# Check if the given file is YAML
if pipelineFileName.endswith('.yaml') or pipelineFileName.endswith('yml'):
    print("The given file is valid.\n")
else:
    print("The file: {} is not an valid YAML file.\n")

# Opening the given Pipeline file
with open(os.path.join(sys.path[0], pipelineFileName), "r") as f:
    data = yaml.load(f, Loader=SafeLoader)

# Declarations
apiVersion = data['apiVersion']
kind = data['kind']
metadataName = data['metadata']['name']

# Checks for apiVersion and kind
if apiVersion == 'tekton.dev/v1alpha1' or apiVersion == 'tekton.dev/v1beta1':
    print("The apiVersion specified is: {}".format(apiVersion))
else:
    print("The given apiVersion is not valid.")

if kind == "Pipeline":
    print("The kind specified is      : {}\n".format(kind))
else:
    print("The given kind is not an Pipeline.\n")

# Truncate file if already exists or create and append to this file
if os.path.isfile(os.path.join(sys.path[0], "{}-mermaid-schema.md".format(metadataName))):
    f = open(os.path.join(sys.path[0], "{}-mermaid-schema.md".format(metadataName)), "r+")
    f.truncate(0)
    f.close()

# Open file based on Pipeline name and fill in the basic settings
f = open(os.path.join(sys.path[0], "{}-mermaid-schema.md".format(metadataName)), "a")
f.write("```mermaid\n")
f.write("graph TD;\n")
f.write("\n")
f.write("classDef ClusterTask fill:#8d8,stroke:#000,stroke-width:0.5pt;\n")
f.write("\n")

# Write used Tasks in Pipeline
for task in data['spec']['tasks']:
    f.write("{}([{} - {} - {}]):::{}\n".format(task['name'], task['name'], task['taskRef']['kind'], task['taskRef']['name'], task['taskRef']['kind']))

f.write("\n")

# Check runAfter confition and add to .md file
for task in data['spec']['tasks']:
    if 'runAfter' in task:
        for runAfter in task['runAfter']:
            f.write("{} --> {}\n".format(runAfter, task['name']))

f.write("\n")
f.write("```")

print("The Mermaid schema is succesfully generated.")